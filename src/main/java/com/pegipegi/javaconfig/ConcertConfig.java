/**
 * 
 */
package com.pegipegi.javaconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.pegipegi.concert.Audience;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages={"com.pegipegi.concert"})
public class ConcertConfig {

	@Bean
	public Audience audience() {
		return new Audience();
	}
	
}
