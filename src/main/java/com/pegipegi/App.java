package com.pegipegi;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pegipegi.concert.Performance;
import com.pegipegi.javaconfig.ConcertConfig;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConcertConfig.class);
        Performance performance = context.getBean(Performance.class);
        performance.perform();
        context.close();
        
        ClassPathXmlApplicationContext context2 = new ClassPathXmlApplicationContext("aopconfig.xml");
        Performance performance2 = context2.getBean(Performance.class);
        performance2.perform();
        context2.close();
    }
}
